import dictionaryOperations.AnagramDictionaryOperation;
import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.PalindromDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperations;
import fileReader.ResourceInputFileReader;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws IOException {
        //System.out.println("This is my main class");

        //citim de la tastatura
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));//primeste input de la tastatura
        String userInput = in.readLine();//citeste linie cu linie, cuvant cu cuvant

  /*    System.out.println("User input: " +userInput);
        userInput = in.readLine();
        System.out.println("Second line: " +userInput);*/

        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");//citim dintr-un fisier care returnrza o lista de stringuri
        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);//dupa ce facem removeDuplicate la words ramane Set

//        for(String word:words){ parcurge cuvintele din dex.txt si le afiseaza
//            System.out.println(word);
//        }

//        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);
//        for (String word : wordsNoDuplicate) {
//            System.out.println(word);
//        }

        //atata timp cat primeste un input de la tastatura,putem accesa un meniu:
        while (true) {
            System.out.println("Menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes\n" +
                    "3. Anagrame\n" +
                    "0. Exit");

            userInput = in.readLine();//
            if (userInput.equals("0")) {
                System.out.println("La revedeere!");
                break;
            }

            DictionaryOperation operation = null;//declaram un operation generic

            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperations(wordsNoDuplicate, in);//specificam tipul operation
                    break;
                case "2":
                    operation = new PalindromDictionaryOperation((wordsNoDuplicate));
                    break;
                case "3":
                    operation = new AnagramDictionaryOperation(wordsNoDuplicate, in);
                    break;
                default:
                    System.out.println("Invalid option!");
            }
            if (operation != null) {
                operation.run();
            }
        }


    }
}
