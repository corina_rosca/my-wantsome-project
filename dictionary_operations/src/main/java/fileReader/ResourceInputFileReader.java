package fileReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ResourceInputFileReader {
    public List<String> readFile(String input) throws IOException {
        List<String> result = new ArrayList<>();//lista unde salvam fiecare cuvant din fisierul dex.txt

        ClassLoader classLoader = ResourceInputFileReader.class.getClassLoader();//formeaza calea
        URL resource = classLoader.getResource(input);//sursa fisierului dex.txt
        File file;

        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {//BufferReader primeste info dintr-un fisier-reader

            String line;
            while ((line = br.readLine()) != null) {//citim fiecare cuvant linie cu linie si adaugam in lista noastra
                result.add(line);
            }
        }

        return result;
    }
}
