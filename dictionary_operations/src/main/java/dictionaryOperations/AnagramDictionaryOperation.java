package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnagramDictionaryOperation implements DictionaryOperation {


    private Set<String> wordsSet;
    private BufferedReader in;


    public AnagramDictionaryOperation(Set<String> wordsSet, BufferedReader in) {
        this.in = in;
        this.wordsSet = wordsSet;

    }

    @Override
    public void run() throws IOException {
        System.out.println("Anagrams searching...");
        Map<String, List<String>> anagrams = Utils.findAllAnagrams(wordsSet);

        String userInput = in.readLine();
        String sortedInput = Utils.sorttedLetters(userInput).toLowerCase();

        if (anagrams.containsKey(sortedInput)) {
            List<String> anagramsInput = anagrams.get(sortedInput);
            System.out.println("Anagrams for a given word: " + userInput + ":" + anagramsInput);
        } else {
            System.out.println("No anagrams for this word: " + "" + userInput);
        }
        System.out.println("Done!");

    }
}






