package dictionaryOperations;

import utils.Utils;

import java.io.IOException;
import java.util.Set;

public class PalindromDictionaryOperation implements DictionaryOperation {

    private Set<String> wordSet;

    public PalindromDictionaryOperation(Set<String> wordSet) {
        this.wordSet = wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Palindromes....");

        Set<String> palindromes = Utils.findAllPalindromes(wordSet);
        for (String line : palindromes) {
            System.out.println(line);
        }
        System.out.println("Donee!");
    }
}
