package dictionaryOperations;
//find all words which contains a specific substring

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;

public class SearchDictionaryOperations implements DictionaryOperation {

    private Set<String> wordSet;
    private BufferedReader in;//scriem un cuvant de la tastatura

    public SearchDictionaryOperations(Set<String> wordSet, BufferedReader in) {
        this.in = in;
        this.wordSet = wordSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Searching...");
        String userInput = in.readLine();//citeste de la tastatura input-ul
        System.out.println("From user: " + userInput);

        Set<String> result = Utils.findWordsThatContainsASpecificString(wordSet, userInput);

        for (String line : result) {
            System.out.println(line);
        }
        System.out.println("Finished!");
    }
}
