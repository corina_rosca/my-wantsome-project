package dictionaryOperations;

import java.io.IOException;

public interface DictionaryOperation {
    void run() throws IOException;
}
