package utils;

import java.util.*;

public class Utils {

    public static Set<String> removeDuplicates(List<String> allLines) {//primeste ca parametru o lista de stringuri si returneaza un set de stringuri

        Set<String> wordSet = new HashSet<>();
        for (String line : allLines) {//parcurgem lista de cuvinte
            wordSet.add(line);
        }
        return wordSet;
    }

    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordSet, String word) {

        Set<String> result = new HashSet<>();//returnam un set de stringuri care contin stringul pe care il dam ca input
        for (String line : wordSet) {
            if (line.contains(word)) {
                result.add(line);//adaugam in lista result cuvantul dat de la tastatura
            }
        }
        return result;
    }
    //  ex of palindromes:ana,unu

    public static Set<String> findAllPalindromes(Set<String> wordsSet) {

        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();// dam un cuvant din dictionar si facem reverse
            if (line.equals(reverseWord.toString())) {
                result.add(line);

            }
        }
        return result;//returnam lista noastra de palindroame
    }

    public static String sorttedLetters(String word) {
        //convert input string to char array
        char myArray[] = word.toCharArray();

        // sort myArray
        Arrays.sort(myArray);

        String sortCharacters = new String(myArray);

        // return new sorted string
        return sortCharacters;
    }

    public static Map<String, List<String>> findAllAnagrams(Set<String> wordset) {
        Map<String, List<String>> anagrams = new HashMap<>();

        if ((wordset == null || wordset.size() == 0)) {
            return null;
        }
        for (String word : wordset) {//sort the letters in the word
            char[] charArray = word.trim().toCharArray();
            Arrays.sort(charArray);
            String sortedWord = String.valueOf(charArray);
            if (anagrams.containsKey(sortedWord)) {//If the map has a key with this sorted word, add the original word to the value list
                List<String> valuesList = anagrams.get(sortedWord);
                valuesList.add(word);
                anagrams.put(sortedWord, valuesList);
            } else {//If the map doesn't have this key, create a new list, add the word to it, and add it to the map
                List<String> newList = new ArrayList<String>();
                newList.add(word);
                anagrams.put(sortedWord, newList);
            }
        }
        return anagrams;

    }
}











