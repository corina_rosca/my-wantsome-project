import fileReader.ResourceInputFileReader;
import org.junit.Assert;
import org.junit.Test;
import utils.Utils;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.*;

public class UtilsTests {
    @Test
    public void testPalindromFinder() {
        Set<String> inputSet = new HashSet<>();//primeste o lista de palindroame,alese de noi
        inputSet.add("unu");//"unu"ca input si il adaugam
        Set<String> palindromes = Utils.findAllPalindromes(inputSet);//palindromes has"unu"

        assertTrue(palindromes.contains("unu"));
        assertEquals(1, palindromes.size());
        assertTrue(palindromes.size() == 1);
    }

    @Test
    public void testPalindromesFromFile() throws IOException {
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("test.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);

        Set<String> palindromes = Utils.findAllPalindromes(wordsNoDuplicate);

        assertTrue(palindromes.contains("aba"));
        assertFalse(palindromes.contains("trei"));
        assertTrue(palindromes.size() == 3);
    }

    @Test
    public void testPalindromesNoResult() {
        Set<String> inputSet = new HashSet<>();//declaram un set de stringuri ptr input
        inputSet.add("telefon");

        Set<String> palindromes = Utils.findAllPalindromes(inputSet);
        assertTrue(palindromes.size() == 0);
    }


    @Test
    public void testSortedLetters() {
        String word = "veac";
        String sortedWord = Utils.sorttedLetters(word);
        System.out.println(sortedWord);

        assertEquals("acev", sortedWord);
        assertTrue(sortedWord.startsWith("a"));
        assertTrue(sortedWord.endsWith("v"));
    }


    @Test
    public void testAnagramsFinder() {
        Set<String> words = new HashSet<>();
        words.add("mai");
        words.add("iam");
        words.add("argint");
        words.add("lat");
        words.add("atl");
        words.add("port");
        words.add("rpot");
        words.add("loc");
        Map<String, List<String>> anagrams = Utils.findAllAnagrams(words);

        assertTrue(anagrams.containsKey("aim"));
        assertTrue(anagrams.containsKey("clo"));
        assertFalse(anagrams.containsKey("port"));
        assertTrue(anagrams.containsKey("oprt"));
        assertFalse(anagrams.values().containsAll(words));
        assertEquals(5, anagrams.size());
    }


    @Test
    public void testAnagramsFromFile() throws IOException {
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        List<String> words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);
        Map<String, List<String>> anagrams = Utils.findAllAnagrams(wordsNoDuplicate);

        for (Map.Entry<String, List<String>> entry : anagrams.entrySet()) {
            String key = entry.getKey();
            System.out.println(key);
            List<String> value = entry.getValue();
            System.out.println(value);
        }
        int size = anagrams.size();
        System.out.println(size);
        Assert.assertEquals(22562, anagrams.size());

        Assert.assertTrue(anagrams.containsKey("iimoz"));
        Assert.assertTrue(anagrams.containsKey("ceh"));
        Assert.assertFalse(anagrams.values().containsAll(words));
        Assert.assertTrue(anagrams.values().toString().contains("[lainică, ciliană]"));

    }


}

